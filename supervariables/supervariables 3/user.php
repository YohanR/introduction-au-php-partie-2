<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Titre de ma page</title>
    </head>
    <body>
    <p>
        Salut <?php echo $_SESSION['login']; ?> !<br />
        Tu as <?php echo $_SESSION['mdps']; ?> <br>
    </p>
    </body>
</html>
